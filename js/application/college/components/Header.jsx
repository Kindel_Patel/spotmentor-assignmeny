'use strict';

import React from 'react';
import AppBar from 'material-ui/AppBar';

class Header extends React.Component {

	constructor(props) {
        super(props);
    }

    render() {
        return (
        	<div className="fixed-header">
            	<AppBar title="Spot Mentor"/>
            </div>
            );
    }
}

export default Header;
