'use strict';

import React from 'react';

class Footer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="footer-style">
              <span>{ '\u00A9 2017 Spot Mentor, All rights reserved.' }</span>
            </footer>
            );
    }
}

export default Footer;
