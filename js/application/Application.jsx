'use strict';

import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {List, ListItem} from 'material-ui/List';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import event from 'Event';
import _ from 'lodash';
import UI from 'UI';
import Chart from 'chart.js';
import {Bar as BarChart} from 'react-chartjs';

import Header from './college/components/Header.jsx';
import Footer from './college/components/Footer.jsx';
import Main from './college/components/Main.jsx';

var data = require('./lists.json');

class Application extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            myData: data, 
            studentList: '', 
            studentDetail: '', 
            fieldText: null, 
            marks: '', 
            chartOption: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            },
            chartData: ''
        };
    }

    getChildContext() {
        return {muiTheme: getMuiTheme(lightTheme)};
    }

    _findList(list, item) {
        this.setState({studentList: item[list]});
        this.setState({studentDetail: ''});
    }

    _findStudentDetail(sitem) {
        this.setState({studentDetail: sitem});
        this.setState({marks: this.state.studentDetail.Marks});
        var temp = this.state.marks;
        this.setState({
            chartData: {
                labels: ["Maths", "Science", "English"],
                datasets: [{
                    label: '# of Votes',
                    data: [temp.Maths, temp.Science, temp.English],
                    backgroundColor: [
                        'rgba(255, 73, 51,1)',
                        'rgba(168, 255, 51,1)',
                        'rgba(51, 150, 255,1)'
                    ],
                    borderColor: [
                        'rgba(255, 73, 51,1)',
                        'rgba(168, 255, 51,1)',
                        'rgba(51, 150, 255,1)'
                    ],
                    borderWidth: 1
                }]
            }
        });
    }

    _searchedText(e) {
        this.setState({fieldText: e.target.value});
    }

    render() {
        var menuItems = this.state.myData.map(function(item, j){
            var keys = Object.keys(item);
            return(
                keys.map(function(list){
                    return(<MenuItem onClick={this._findList.bind(this, list, item)}>{list}</MenuItem>)
                }, this)
            )
        }, this)

        var studentItems;
        if(this.state.studentList != '' && this.state.studentList != undefined) {
            studentItems = this.state.studentList.map(function(sitem, k){
                if(sitem.Name.search(this.state.fieldText) >= 0 || this.state.fieldText == null) {
                    return(
                        <ListItem onClick={this._findStudentDetail.bind(this, sitem)}>{sitem.Name}</ListItem>
                    )
                }
            }, this)
        }

        return (
            <div>
                <Header/>
                <div className="fluid-row main-content">
                    <div className="col-3">
                        <Drawer className="left-panel">
                            {menuItems}
                        </Drawer>
                    </div>
                    <div className="col-9">
                        <div className="fluid-row right-container">
                            <div className="col-6">
                                {(this.state.studentList == '' || this.state.studentList == undefined) 
                                    ? 
                                        <Main/> 
                                    : 
                                        <Card className="student-list">
                                            <div className="search-field"><TextField hintText="Search Name" fullWidth="true" floatingLabelFixed="false" value={ this.state.searchName } onChange = {this._searchedText.bind(this)}/></div>
                                            <List>{studentItems}</List>
                                        </Card>
                                }
                            </div>
                            <div className="col-6">
                             {(this.state.studentDetail == '' || this.state.studentDetail == undefined) 
                                ?
                                    <div></div>
                                :
                                    <div>
                                        <Card>
                                            <List>
                                                <ListItem>D.O.B: {this.state.studentDetail.DOB}</ListItem>
                                                <ListItem>Gender: {this.state.studentDetail.Gender}</ListItem>
                                                <ListItem>Parent/guardian: {this.state.studentDetail.Parent}</ListItem>
                                            </List>
                                        </Card>
                                        <BarChart data={this.state.chartData} options={this.state.chartOption} width="300" height="250"/>
                                    </div>
                            }
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
            );
    }
}


Application.childContextTypes = {
    muiTheme: React.PropTypes.object
};

export default Application;
