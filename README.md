# My project's README
# Spotmentor Assignment README
#Setup Project:
#Extract zip folder
#Copy Extracted folder anywhere.
#Open a command prompt and go to that location.
#Run command "npm install"
#After successful install packages.
#Run command "npm start" and Browse URL "localhost:8080"


#Features:
#On project load showing the Classes of School.
#After Clicking on any class you will get the value of their students.
#After Clicking on any student name you will get its detail.
#There is also a Search Bar in which you can search the name of a student(Case Sensitive). 


#It has also some bugs:
#Marks of student shown after 3rd click on any student name(Only first name).
#The label of the search field is not hiding after text entered on it.
#Different Color is not showing in the bar of the chart.


#Important Notes:
#You must have node installed greater than or equal to version 7.
#You must have npm installed greater than or equal to version 3.5.
#You can change the value of elements by going to file lists.json in js/application/lists.json
